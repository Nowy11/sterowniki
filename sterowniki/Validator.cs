﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sterowniki
{
    public class Validator
    {
        public string Blad { get; set; }

        public bool SprawdzSterownik(object wydzialId, object obiektId, object urzadzenieId, string nrKatalogowy, string opis)
        {
            if (wydzialId == null)
            {
                Blad = "Nieprawidłowe Id wydziału";
                return false;
            }

            if (obiektId == null)
            {
                Blad = "Nieprawidłowe Id obiektu";
                return false;
            }

            if (urzadzenieId == null)
            {
                Blad = "Nieprawidłowe Id urządzenia";
                return false;
            }

            if (string.IsNullOrWhiteSpace(nrKatalogowy))
            {
                Blad = "Nieprawidłowy numer katalogowy";
                return false;
            }
            if (string.IsNullOrWhiteSpace(opis))
            {
                Blad = "Nieprawidłowy opis";
                return false;
            }

            return true;
        }

        
    }
}
