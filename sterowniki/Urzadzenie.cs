﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace sterowniki
{
    public class Urzadzenie
    {
        public int Id { get; set; }
        public string Nazwa { get; set; }
        public string Opis { get; set; }
    }
}
