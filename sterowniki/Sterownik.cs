﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sterowniki
{
    public class Sterownik
    {
        public int Id { get; set; }
        public int WydzialId { get; set; }
        public int ObiektId { get; set; }
        public int UrzadzenieId { get; set; }
        public int Ilosc { get; set; }
        public string Nr_Katalogowy { get; set; }
        public string Nr_Kat_Prefix { get; set; }
        public string Nr_Kat_Rodzina { get; set; }
        public string Nr_Kat_Typ { get; set; }
        public string Nr_Kat_Wersja { get; set; }
        public string Opis { get; set; }

    }
}
