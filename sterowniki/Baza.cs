﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Simple.Data;
using Simple.Data.SqlServer;
using Simple.Data.Ado;

namespace sterowniki
{
    public class Baza
    {
        public string Blad { get; private set; }

        public bool UtworzWydzial(Wydzial wydzial)
        {
            Blad = null;
            try
            {
                var db = Database.Open();
                db.Wydzialy.Insert(wydzial);
            }
            catch (Exception exc)
            {
                Blad = exc.Message;
                return false;
            }

            return true;
        }

        public bool UsunWydzial(int id)
        {
            Blad = null;
            try
            {
                var db = Database.Open();
                db.Wydzialy.DeleteById(id);
            }
            catch (Exception exc)
            {
                Blad = exc.Message;
                return false;
            }

            return true;
        }

        public bool AktualizujWydzial(Wydzial wydzial)
        {
            Blad = null;
            try
            {
                var db = Database.Open();
                db.Wydzialy.UpdateByNazwa(wydzial);
            }
            catch (Exception exc)
            {
                Blad = exc.Message;
                return false;
            }

            return true;
        }

        public bool PobierzWydzialy(out List<Wydzial> wydzialy)
        {
            Blad = null;
            try
            {
                var db = Database.Open();
                wydzialy = db.Wydzialy.All();
            }
            catch (Exception exc)
            {
                Blad = exc.Message;
                wydzialy = null;
                return false;
            }

            return true;
        }


        public bool PobierzWydzial(string nazwa, out Wydzial wydzial)
        {
            Blad = null;
            try
            {
                var db = Database.Open();
                wydzial = db.Wydzialy.FindAllByNazwa(nazwa).FirstOrDefault();
            }
            catch (Exception exc)
            {
                Blad = exc.Message;
                wydzial = null;
                return false;
            }

            return true;
        }

        public bool UtworzUrzadzenie(Urzadzenie urzadzenie)
        {
            Blad = null;
            try
            {
                var db = Database.Open();
                db.Urzadzenia.Insert(urzadzenie);
            }
            catch (Exception exc)
            {
                Blad = exc.Message;
                return false;
            }

            return true;
        }

        public bool UsunUrzadzenie(int id)
        {
            Blad = null;
            try
            {
                var db = Database.Open();
                db.Urzadzenia.DeleteById(id);
            }
            catch (Exception exc)
            {
                Blad = exc.Message;
                return false;
            }

            return true;
        }

        public bool AktualizujUrzadzenie(Urzadzenie urzadzenie)
        {
            Blad = null;
            try
            {
                var db = Database.Open();
                db.Urzadzenia.UpdateByNazwa(urzadzenie);
            }
            catch (Exception exc)
            {
                Blad = exc.Message;
                return false;
            }

            return true;
        }

        public bool PobierzUrzadzenie(string nazwa, out Urzadzenie urzadzenie)
        {
            Blad = null;
            try
            {
                var db = Database.Open();
                urzadzenie = db.Urzadzenia.FindAllByNazwa(nazwa).FirstOrDefault();
            }
            catch (Exception exc)
            {
                Blad = exc.Message;
                urzadzenie = null;
                return false;
            }

            return true;
        }

        public bool PobierzUrzadzenia(out List<Urzadzenie> urzadzenia)
        {
            Blad = null;
            try
            {
                var db = Database.Open();
                urzadzenia = db.Urzadzenia.All();
            }
            catch (Exception exc)
            {
                Blad = exc.Message;
                urzadzenia = null;
                return false;
            }

            return true;
        }

        public bool UtworzObiekt(Obiekt obiekt)
        {
            Blad = null;
            try
            {
                var db = Database.Open();
                db.Obiekty.Insert(obiekt);
            }
            catch (Exception exc)
            {
                Blad = exc.Message;
                return false;
            }

            return true;
        }

        public bool UsunObiekt(int id)
        {
            Blad = null;
            try
            {
                var db = Database.Open();
                db.Obiekty.DeleteById(id);
            }
            catch (Exception exc)
            {
                Blad = exc.Message;
                return false;
            }

            return true;
        }

        public bool AktualizujObiekt(Obiekt obiekt)
        {
            Blad = null;
            try
            {
                var db = Database.Open();
                db.Obiekty.UpdateByNazwa(obiekt);
            }
            catch (Exception exc)
            {
                Blad = exc.Message;
                return false;
            }

            return true;
        }

        public bool PobierzObiekt(string nazwa, out Obiekt obiekt)
        {
            Blad = null;
            try
            {
                var db = Database.Open();
                obiekt = db.Obiekty.FindAllByNazwa(nazwa).FirstOrDefault();
            }
            catch (Exception exc)
            {
                Blad = exc.Message;
                obiekt = null;
                return false;
            }

            return true;
        }

        public bool PobierzObiekty(out List<Obiekt> obiekty)
        {
            Blad = null;
            try
            {
                var db = Database.Open();
                obiekty = db.Obiekty.All();
            }
            catch (Exception exc)
            {
                Blad = exc.Message;
                obiekty = null;
                return false;
            }

            return true;
        }

            public bool UtworzSterownik(Sterownik sterownik)
        {
            Blad = null;
            try
            {
                var db = Database.Open();
                db.Sterowniki.Insert(sterownik);
            }
            catch (Exception exc)
            {
                Blad = exc.Message;
                return false;
            }

            return true;
        }

        public bool UsunSterownik(int id)
        {
            Blad = null;
            try
            {
                var db = Database.Open();
                db.Sterowniki.DeleteById(id);
            }
            catch (Exception exc)
            {
                Blad = exc.Message;
                return false;
            }

            return true;
        }

        public bool AktualizujSterownik(Sterownik sterownik)
        {
            Blad = null;
            try
            {
                var db = Database.Open();
                db.Sterowniki.UpdateById(sterownik);
            }
            catch (Exception exc)
            {
                Blad = exc.Message;
                return false;
            }

            return true;
        }

        public bool PobierzSterownik(int Id, out Sterownik sterownik)
        {
            Blad = null;
            try
            {
                var db = Database.Open();
                sterownik = db.Sterowniki.FindAllById(Id).FirstOrDefault();
            }
            catch (Exception exc)
            {
                Blad = exc.Message;
                sterownik = null;
                return false;
            }

            return true;
        }

        public bool PobierzSterowniki(out List<Sterownik> sterowniki)
        {
            Blad = null;
            try
            {
                var db = Database.Open();
                sterowniki = db.Sterowniki.All();
            }
            catch (Exception exc)
            {
                Blad = exc.Message;
                sterowniki = null;
                return false;
            }

            return true;
        }

        public bool PobierzOstatniSterownik(out Sterownik sterownik)
        {
            Blad = null;
            try
            {
                var db = Database.Open();
                sterownik = db.Sterowniki.All().OrderByIdDescending().FirstOrDefault();
            }
            catch (Exception exc)
            {
                Blad = exc.Message;
                sterownik = null;
                return false;
            }

            return true;
        }
    }
}
