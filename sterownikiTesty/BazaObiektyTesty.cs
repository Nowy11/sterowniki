﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using sterowniki;

namespace sterownikiTesty
{
    public class BazaObiektTest
    {
        Baza baza = new Baza();
        Obiekt obiekt = new Obiekt()
        {
            Nazwa = "TestXUNIT@*",
            Opis = "Test jednostkowy"
            
        };
       

        [Fact]
        public void utworzObiekt_zwraca_true_przy_poprawnym_zapisie()
        {
            bool wynik = false;

            wynik = baza.UtworzObiekt(obiekt);
            Assert.Equal(null, baza.Blad);
            Assert.Equal(true, wynik);

            posprzataj();
            
        }

        [Fact]
        public void aktualizujObiekt_zwraca_true_przy_poprawnym_wykonaniu()
        {
            bool wynik = false;

            przygotuj();

            wynik = baza.AktualizujObiekt(obiekt);

            Assert.Equal(null, baza.Blad);
            Assert.Equal(true, wynik);
            

            posprzataj();
        }

        [Fact]
        public void PobierzObiekty_zwraca_true_i_wydzialy_przy_poprawnym_wykonaniu()
        {
            bool wynik = false;
             
            przygotuj();

            List<Obiekt> obiekty;

            wynik = baza.PobierzObiekty(out obiekty);

            Assert.Null(baza.Blad);
            Assert.Equal(true, wynik);
            Assert.NotNull(obiekty);
            Assert.NotEqual(0, obiekty.Count);

            posprzataj();
        }

        [Fact]
        public void UsunObiekt_zwraca_true_przy_poprawnym_wykonaniu()
        {
            bool wynik = false;
            przygotuj();

            wynik = baza.PobierzObiekt("TestXUNIT@*", out obiekt);

            Assert.Equal(null, baza.Blad);
            Assert.Equal(true, wynik);

            wynik = baza.UsunObiekt(obiekt.Id);
            Assert.Equal(null, baza.Blad);
            Assert.Equal(true, wynik);
        }

        [Fact]
        public void PobierzObiekt_zwraca_true_i_wydzial_przy_poprawnym_wykonaniu()
        {

            bool wynik = false;

            przygotuj();

            wynik = baza.PobierzObiekt("TestXUNIT@*", out obiekt);

            Assert.Null(baza.Blad);
            Assert.NotEqual(null, obiekt);
            Assert.Equal(true, wynik);

            posprzataj();

        }

        private void przygotuj()
        {
            bool wynik = baza.UtworzObiekt(obiekt);
            Assert.Equal(null, baza.Blad);
            Assert.Equal(true, wynik);
        }

        private void posprzataj()
        {
            bool wynik = baza.PobierzObiekt("TestXUNIT@*", out obiekt);

            Assert.Null(baza.Blad);
            Assert.NotEqual(null, obiekt);
            Assert.Equal(true, wynik);

            wynik = baza.UsunObiekt(obiekt.Id);
            Assert.Null(baza.Blad);
            Assert.Equal(true, wynik);
        }
    }
}


