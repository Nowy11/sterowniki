﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using sterowniki;

namespace sterownikiTesty
{
    public class BazaUrzadzeniaTesty
    {
        Baza baza = new Baza();
        Urzadzenie urzadzenie = new Urzadzenie()
        {
            Nazwa = "TestXUNIT@*",
            Opis = "Test jednostkowy"
            
        };
       

        [Fact]
        public void utworzUrzadzenie_zwraca_true_przy_poprawnym_zapisie()
        {
            bool wynik = false;

            wynik = baza.UtworzUrzadzenie(urzadzenie);
            Assert.Equal(null, baza.Blad);
            Assert.Equal(true, wynik);

            posprzataj();
            
        }

        [Fact]
        public void aktualizujUrzadzenie_zwraca_true_przy_poprawnym_wykonaniu()
        {
            bool wynik = false;

            przygotuj();

            wynik = baza.AktualizujUrzadzenie(urzadzenie);

            Assert.Equal(null, baza.Blad);
            Assert.Equal(true, wynik);
            

            posprzataj();
        }

        [Fact]
        public void PobierzUrzadzania_zwraca_true_i_wydzialy_przy_poprawnym_wykonaniu()
        {
            bool wynik = false;

            przygotuj();

            List<Urzadzenie> urzadzenia;

            wynik = baza.PobierzUrzadzenia(out urzadzenia);

            Assert.Null(baza.Blad);
            Assert.Equal(true, wynik);
            Assert.NotNull(urzadzenia);
            Assert.NotEqual(0, urzadzenia.Count);

            posprzataj();
        }

        [Fact]
        public void UsunUrzadzenie_zwraca_true_przy_poprawnym_wykonaniu()
        {
            bool wynik = false;
            przygotuj();

            wynik = baza.PobierzUrzadzenie("TestXUNIT@*", out urzadzenie);

            Assert.Equal(null, baza.Blad);
            Assert.Equal(true, wynik);

            wynik = baza.UsunUrzadzenie(urzadzenie.Id);
            Assert.Equal(null, baza.Blad);
            Assert.Equal(true, wynik);
        }

        [Fact]
        public void PobierzUrzadzenie_zwraca_true_i_wydzial_przy_poprawnym_wykonaniu()
        {

            bool wynik = false;

            przygotuj();

            wynik = baza.PobierzUrzadzenie("TestXUNIT@*", out urzadzenie);

            Assert.Null(baza.Blad);
            Assert.NotEqual(null, urzadzenie);
            Assert.Equal(true, wynik);

            posprzataj();

        }

        private void przygotuj()
        {
            bool wynik = baza.UtworzUrzadzenie(urzadzenie);
            Assert.Equal(null, baza.Blad);
            Assert.Equal(true, wynik);
        }

        private void posprzataj()
        {
            bool wynik = baza.PobierzUrzadzenie("TestXUNIT@*", out urzadzenie);

            Assert.Null(baza.Blad);
            Assert.NotEqual(null, urzadzenie);
            Assert.Equal(true, wynik);

            wynik = baza.UsunUrzadzenie(urzadzenie.Id);
            Assert.Null(baza.Blad);
            Assert.Equal(true, wynik);
        }
    }
}
