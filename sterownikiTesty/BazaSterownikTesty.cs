﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using sterowniki;

namespace sterownikiTesty
{
    public class BazaSterownikTesty
    {
        Baza baza = new Baza();
        Sterownik sterownik = new Sterownik()
        {
            ObiektId = 99,
            WydzialId = 0,
            UrzadzenieId = 124,
            Ilosc = 1,
            Nr_Katalogowy = "numer kat",
            Nr_Kat_Prefix = "prefix",
            Nr_Kat_Rodzina = "rodzina",
            Nr_Kat_Typ = "typ",
            Nr_Kat_Wersja = "wersja",
            Opis = "Test jednostkowy"

        };


        [Fact]
        public void utworzSterownik_zwraca_true_przy_poprawnym_zapisie()
        {
            bool wynik = false;
            wynik = baza.UtworzSterownik(sterownik);
            Assert.Equal(null, baza.Blad);
            Assert.Equal(true, wynik);

            posprzataj();

        }

        [Fact]
        public void aktualizujSterownik_zwraca_true_przy_poprawnym_wykonaniu()
        {
            bool wynik = false;

            przygotuj();

            wynik = baza.PobierzOstatniSterownik(out sterownik);

            Assert.Null(baza.Blad);
            Assert.NotEqual(null, sterownik);
            Assert.Equal(true, wynik);
           
            wynik = baza.AktualizujSterownik(sterownik);

            Assert.Equal(null, baza.Blad);
            Assert.Equal(true, wynik);


            posprzataj();
        }

        [Fact]
        public void PobierzSterowniki_zwraca_true_i_sterowniki_przy_poprawnym_wykonaniu()
        {
            bool wynik = false;

            przygotuj();

            List<Sterownik> sterowniki;

            wynik = baza.PobierzSterowniki(out sterowniki);

            Assert.Null(baza.Blad);
            Assert.Equal(true, wynik);
            Assert.NotNull(sterowniki);
            Assert.NotEqual(0, sterowniki.Count);

            Console.WriteLine(sterowniki[0].Id);

            posprzataj();
        }

        [Fact]
        public void UsunSterownik_zwraca_true_przy_poprawnym_wykonaniu()
        {
            bool wynik = false;
            przygotuj();

            wynik = baza.PobierzOstatniSterownik(out sterownik);

            Assert.Equal(null, baza.Blad);
            Assert.Equal(true, wynik);

            wynik = baza.UsunSterownik(sterownik.Id);
            Assert.Equal(null, baza.Blad);
            Assert.Equal(true, wynik);
        }

        [Fact]
        public void PobierzSterownik_zwraca_true_i_wydzial_przy_poprawnym_wykonaniu()
        {

            bool wynik = false;

            przygotuj();

            wynik = baza.PobierzOstatniSterownik( out sterownik);

            Assert.Null(baza.Blad);
            Assert.NotEqual(null, sterownik);
            Assert.Equal(true, wynik);

            posprzataj();

        }

        private void przygotuj()
        {
            bool wynik = baza.UtworzSterownik(sterownik);
            Assert.Equal(null, baza.Blad);
            Assert.Equal(true, wynik);
        }

        private void posprzataj()
        {
            bool wynik = baza.PobierzOstatniSterownik(out sterownik);

            Assert.Null(baza.Blad);
            Assert.NotEqual(null, sterownik);
            Assert.Equal(true, wynik);

            wynik = baza.UsunSterownik(sterownik.Id);
            Assert.Null(baza.Blad);
            Assert.Equal(true, wynik);
        }
    }
}

