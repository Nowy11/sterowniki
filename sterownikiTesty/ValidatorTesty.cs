﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using sterowniki;

namespace sterownikiTesty
{
    public class ValidatorTesty
    {
        Validator validator = new Validator();
        [Fact]
        public void SprawdzSterownik_zwraca_false_jezeli_dowolne_pole_jest_niewypelnione()
        {
            bool wynik = false;
            wynik = validator.SprawdzSterownik(1, null, 3, "1wsdsa", "opis");

            Assert.False(wynik);
        }
        [Fact]
        public void SprawdzSterownik_zwraca_true_jezeli_wszystkie_pola_sa_wypelnione()
        {
            bool wynik = false;
            wynik = validator.SprawdzSterownik(1, 2, 3, "nr.Kat", "opis");

            Assert.True(wynik);
        }

        
    }
}
