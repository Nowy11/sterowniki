﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using sterowniki;

namespace sterownikiTesty
{
    public class BazaWydzialTesty
    {
        Baza baza = new Baza();
        Wydzial wydzial = new Wydzial()
        {
            Nazwa = "TestXUNIT@*",
            Opis = "Test jednostkowy"

        };

        [Fact]
        public void utworzWydzial_zwraca_true_przy_poprawnym_zapisie()
        {
            bool wynik = false;

            wynik = baza.UtworzWydzial(wydzial);
            Assert.Equal(null, baza.Blad);
            Assert.Equal(true, wynik);

            posprzataj();

        }

        [Fact]
        public void aktualizujWydzial_zwraca_true_przy_poprawnym_wykonaniu()
        {
            bool wynik = false;

            przygotuj();

            wynik = baza.AktualizujWydzial(wydzial);

            Assert.Equal(null, baza.Blad);
            Assert.Equal(true, wynik);

            posprzataj();
        }

        [Fact]
        public void PobierzWydzialy_zwraca_true_i_wydzialy_przy_poprawnym_wykonaniu()
        {
            bool wynik = false;

            przygotuj();

            List<Wydzial>wydzialy;

            wynik = baza.PobierzWydzialy(out wydzialy);

            Assert.Null(baza.Blad);
            Assert.Equal(true, wynik);
            Assert.NotNull(wydzialy);
            Assert.NotEqual(0, wydzialy.Count);

            posprzataj();
        }

        [Fact]
        public void UsunWydzial_zwraca_true_przy_poprawnym_wykonaniu()
        {
            bool wynik = false;
            przygotuj();

            wynik = baza.PobierzWydzial("TestXUNIT@*", out wydzial);

            Assert.Equal(null, baza.Blad);
            Assert.Equal(true, wynik);

            wynik = baza.UsunWydzial(wydzial.Id);
            Assert.Equal(null, baza.Blad);
            Assert.Equal(true, wynik);
        }

        [Fact]
        public void PobierzWydzial_zwraca_true_i_wydzial_przy_poprawnym_wykonaniu()
        {
           
            bool wynik = false;

            przygotuj();

            wynik = baza.PobierzWydzial("TestXUNIT@*", out wydzial);

            Assert.Equal(null, baza.Blad);
            Assert.NotEqual(null, wydzial);
            Assert.Equal(true, wynik);

            posprzataj();

        }

        private void przygotuj()
        {
            bool wynik = baza.UtworzWydzial(wydzial);
            Assert.Equal(null, baza.Blad);
            Assert.Equal(true, wynik);
        }

        private void posprzataj()
        {
            bool wynik = baza.PobierzWydzial("TestXUNIT@*", out wydzial);

            Assert.Equal(null, baza.Blad);
            Assert.NotEqual(null, wydzial);
            Assert.Equal(true, wynik);

            wynik = baza.UsunWydzial(wydzial.Id);
            Assert.Equal(null, baza.Blad);
            Assert.Equal(true, wynik);
        }
    }
}
